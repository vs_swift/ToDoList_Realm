import UIKit

class AddItemController: UIViewController {
  
  @IBOutlet weak var taskNameTextField: UITextField!
  @IBOutlet weak var taskDescTextField: UITextField!
  
  private var newTask: TaskObject?
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  @IBAction func tapAddButton(_ sender: Any) {
    newTask = TaskObject()
    newTask!.taskName = taskNameTextField.text!
    newTask!.taskDescription = taskDescTextField.text!
    Storage.realm!.beginWrite()
    Storage.shared.realmRecords!.first?.taskList.append(newTask!)
//    Storage.realm!.add(Storage.shared.realmRecords!)
    try! Storage.realm!.commitWrite()
    
    dismiss(animated: true, completion: nil)
  }
  @IBAction func tapCancel(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
}
