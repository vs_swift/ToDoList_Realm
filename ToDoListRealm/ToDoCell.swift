import UIKit

class ToDoCell: UITableViewCell {
  
  @IBOutlet weak var itemNameLabel: UILabel!
  @IBOutlet weak var checkMark: UIImageView!
  private var isCompleted: Bool = true
  
  func setData(_ data: TaskObject) {
    self.itemNameLabel.text = data.taskName
    checkMark.isHidden = !data.isCompleted
  }
}
