import UIKit

class ListController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  
  struct Segues {
    static let detailsSegue = "showDetailsSegue"
  }
  
  struct EditButtonText {
    static let edit = "Edit"
    static let done = "Done"
  }
  @IBOutlet weak var editBarButton: UIBarButtonItem!
  @IBOutlet weak var addBarButton: UIBarButtonItem!
  @IBOutlet weak var toDoListTable: UITableView!
  private var storedObjects = Storage.shared.realmRecords?.first?.taskList
  private var selectedRowIndex: Int?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    toDoListTable.delegate = self
    toDoListTable.dataSource = self
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    toDoListTable.reloadData()
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return storedObjects!.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ToDoCell.self), for: indexPath) as! ToDoCell
    print("Index:", indexPath.row)
    let data = storedObjects![indexPath.row]
    cell.setData(data)
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
    selectedRowIndex = indexPath.row
    return indexPath
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == ListController.Segues.detailsSegue
    {
      let formatter = DateFormatter()
      formatter.dateStyle = .full
      let destination = segue.destination as! TaskDetailsController
      let data = storedObjects![selectedRowIndex!]
      let creationDateString = formatter.string(from: data.createDate as Date)
      destination.name = data.taskName
      destination.desc = data.taskDescription
      destination.taskObject = data
      destination.dateCreated = creationDateString
    }
  }
  
  @IBAction func tapEdit(_ sender: Any) {
    toDoListTable.setEditing(!toDoListTable.isEditing, animated: true)
    if toDoListTable.isEditing == false {
      editBarButton.title = ListController.EditButtonText.edit
      addBarButton.isEnabled = true
    } else {
      editBarButton.title = ListController.EditButtonText.done
      addBarButton.isEnabled = false
    }
  }
  
  func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
    Storage.realm?.beginWrite()
    let movedObject = storedObjects![sourceIndexPath.row]
    storedObjects!.remove(at: sourceIndexPath.row)
    storedObjects!.insert(movedObject, at: destinationIndexPath.row)
    try! Storage.realm?.commitWrite()
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
      Storage.realm!.beginWrite()
      storedObjects!.remove(at: indexPath.row)
      try! Storage.realm!.commitWrite()
      tableView.reloadData()
    }
  }
}
