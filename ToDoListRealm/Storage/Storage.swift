import RealmSwift

class Storage {
  static let shared = Storage()
  static var realm: Realm?
  
  var realmRecords: Results<TaskList>?
  
  init() {
    let config = Realm.Configuration(deleteRealmIfMigrationNeeded: true)
    Realm.Configuration.defaultConfiguration = config
    
    Storage.realm = try! Realm()
    realmRecords = Storage.realm!.objects(TaskList.self)
    
    if realmRecords?.count == 0 {
      Storage.realm?.beginWrite()
      Storage.realm?.add(TaskList())
      try! Storage.realm?.commitWrite()
      realmRecords = Storage.realm!.objects(TaskList.self)
    }
//    Uncomment this lines to print path of Realm file location
//    let url = Storage.realm!.configuration.fileURL
//    print("url is: \(url)")
  }
}
