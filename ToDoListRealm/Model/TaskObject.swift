import RealmSwift
import Foundation

class TaskObject: Object {
  @objc dynamic var taskName: String = ""
  @objc dynamic var createDate = NSDate()
  @objc dynamic var taskDescription: String = ""
  @objc dynamic var isCompleted: Bool = false
}
