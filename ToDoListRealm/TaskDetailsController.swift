import UIKit

class TaskDetailsController: UIViewController {
  var name: String = ""
  var desc: String = ""
  var dateCreated: String = ""
  var taskObject: TaskObject?
  
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var taskNameLabel: UILabel!
  @IBOutlet weak var taskDesc: UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    taskNameLabel.text = name
    taskDesc.text = desc
    dateLabel.text = dateCreated
  }
  
  @IBAction func markNotCompleted(_ sender: Any) {
    Storage.realm!.beginWrite()
    taskObject?.isCompleted = false
    Storage.realm!.add(taskObject!)
    try! Storage.realm!.commitWrite()
  }
  
  @IBAction func markCompleted(_ sender: Any) {
    Storage.realm!.beginWrite()
    taskObject?.isCompleted = true
    Storage.realm!.add(taskObject!)
    try! Storage.realm!.commitWrite()
  }
}
